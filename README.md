# HTTP server

for running server run http.py with the port number as command line argument
e.g.

python3 http.py 31180

for running testing file 1st run server file and then run the testing file with the port number as command line argument
this port number should be same as port number of server

command to run test file

python3 testing.py 31180
