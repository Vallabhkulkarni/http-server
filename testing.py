from re import M
import requests
import sys
import mimetypes
import threading

port = int(sys.argv[1])

url1 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/vallabh.html'
url2 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/nofile.html'
url5 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/1.png'
url3 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/vallabh.html'

urld = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/dlt.txt'
urld1 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test'
urld2 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/example_1.json'

puturl = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/put.txt'
puturl1 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/put.png'
puturl2 = 'http://127.0.0.1:' +str(port) +'/home/shreyas/Desktop/test/put.pdf'

headers = {'Host': 'localhost','user-agent': 'shreyas-/0.0.1','Connection': 'Close','If-Modified-Since': 'Tue, 14 Nov 2021 10:26:59  GMT'}
headers1 = {'Host': 'localhost','user-agent': 'shreyas-/0.0.1','Connection': 'Close','If-Unmodified-Since': 'Tue, 10 Nov 2021 10:26:59  GMT'}


def get_Responses():
	print("----------------------GET responseses-------------------")
	print()
 
	print("------Ok Response---------")
	get1 = requests.get(url1)
	print(get1.headers)
	print(get1.status_code)
	print()
 
	print("------If Modified---------")
	get2 = requests.get(url1, headers=headers)
	print(get2.headers)
	print(get2.status_code)
	print()
 
	print("------image---------")
	get3 = requests.get(url5, headers=headers)
	print(get3.headers)
	print(get3.status_code)
	print(get3.content)
	print()
 
	print("------If Unmodified---------")
	get4 = requests.get(url1, headers=headers1)
	print(get4.headers)
	print(get4.status_code)
	print(get4.content)
	print()
 
	print("------Bad Request---------")
	get5 = requests.get(url3)
	print(get5.headers)
	print(get5.status_code)
	print()
 
	return

def Head() :
	print("----------------------HEAD responseses-------------------")
	print("------Ok Response---------")
	head1 = requests.head(url1)
	headers = head1.headers
	for key, value in headers.items():
		print(f'{key}: {value}')
	print(head1.status_code)
	print()
 
	print("------Not Found---------")
	head2 = requests.get(url2)
	print(head2.headers)
	print(head2.status_code)
	print()
 
	print("------Bad Request---------")
	head3 = requests.get(url3)
	print(head3.headers)
	print(head3.status_code)
	print()
 
	return


def Delete():
	print("----------------------DELETE responseses-------------------")
	print("------Ok Response---------")
	delete1 = requests.delete(urld)
	print(delete1.headers)
	print(delete1.status_code)
	print()
	print("------Forbidden dur to directory can't delete---------")
	delete2 = requests.delete(urld1)
	print(delete2.headers)
	print(delete2.status_code)
	print()
	print("------Not Found---------")
	delete4 = requests.delete(url2)
	print(delete4.headers)
	print(delete4.status_code)
	print()
 
	return


def put_txt():
	print("put request for txt")
	f = open("/home/shreyas/Desktop/test/put.txt","rb")
	filename = "put.txt"
	data = f.read()
	content_type = mimetypes.guess_type(filename)[0]
	put1 = requests.put(puturl, data=data, headers={'Content-Type': content_type})
	put1.encoding = 'utf-8'
	print(put1.status_code)
 
	return
#getting some module erros while excution
"""
def put_pdf():
	print("put request for binary")
	f = open("/home/shreyas/Desktop/test/put.pdf","rb")
	filename = "put.pdf"
	content_type = mimetypes.guess_type(filename)[0]
	data = f.read()
	put2 = requests.put(puturl2, data=data, headers={'Content-Type': content_type})
	put2.encoding = 'utf-8'
	print(put2.status_code)
 
	return
"""
def post_file():
    
    filename = '1.png'
    f = open(filename, 'rb')

    files = {'fileToUpload': f}
    url='http://127.0.0.1:' + str(port) + '/home/shreyas/Desktop/test/example_2.json'
    response = requests.post(url=url, files=files)
    response.encoding = 'utf-8'
    print(response.status_code)
    
    return


def post():
    data = {"name": "vallabh", "mis": "111903118"}
    url='http://127.0.0.1:' + str(port) + '/home/shreyas/Desktop/test/example_1.json'
   
    response = requests.post(url=url, data=data)
    response.encoding = 'utf-8' 
    print(response.status_code)
    
    return
"""  
def MaxRequests(num):
	get100 = []
	try:
		for i in range(num):
			get100[i] = threading.Thread(get_Responses()).start()
           
	except:
		print("Exception. Server has stopped working")
		return
"""
#MaxRequests(10)

get_Responses()

Head()

Delete()


get_Responses()

Head()

Delete()
   
put_txt()
#put_pdf()
    
post_file()
post()
