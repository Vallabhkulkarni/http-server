from socket import *
import threading
from time import gmtime, strftime
import time, datetime
import os
import sys
import json
import random
import string
import re

limit = 1024
conn_available = 0 #max connection is 10
#port = 31180
config ={}
config["ServerRoot"] = "/home/shreyas/Desktop/http"
#maximum connections possible at time 
config["MaxRequests"] = 25
#if port number is not given through command line then set default port 
config["Listen"] = 31180

try:
	conf_file = open("http.conf", "r")
	lines = conf_file.readlines()
	for i in lines:
		#ignore the lines starting with # which are comments and empty lines too
		if(i[0] == "#" or i[0] == "\n"):
			continue
		key = re.split(' |\n',i)[0]
		value = re.split(' |\n',i)[1]
		config[key] = value
	conf_file.close()
except:
	print("error")

portno = int(config["Listen"])


#when port is given through command line argument it take the port given else set default port 

if (len(sys.argv) == 1):
	port = portno
	print("Default port set")
	print(port)
elif (len(sys.argv) == 2):
	port = int(sys.argv[1])
else:
	print("Invalid Arguments.")

try:
	serversocket = socket(AF_INET,SOCK_STREAM)
#	current_time = datetime.datetime.now
	print("Server socket created on :",end='')
#	print(current_time)
except:
	print("Error in creating Server socket")
	exit()

try:
	serversocket.bind(('',port))
	print("Binded to port number: ", port)
except:
	print("Port is busy")
	exit()

serversocket.listen(5)
print("socket is listening")

#status code dictionary
status = { 200:'OK',
201:'Created',
300:'Multiple Choices',
301:'Moved Permanently',
302:'Found',
304:'Not Modified',
307:'Temporary Redirect',
400:'Bad request',
401:'Unauthorized',
403:'Foridden',
404:'Not Found',
411:'Length Required',
412:'Precondition Failed',
415:'Unsupported Media Type',
500:'Internal Server Error',
501:'Not Implemented',
503:'Service Unavailable',
550:'Permission denied'
}


#return current date and time
def current_datetime():
	return time.strftime("%a, %d %b %Y %H:%M:%S  %Z", time.gmtime())
def modified_datetime(file_name):
	return time.strftime("%a, %d %b %Y %H:%M:%S  %Z", time.gmtime(os.path.getmtime(file_name)))


#gives content type 
def content_type(ext):
	application=["pdf","zip", "json"]
	text=["css", "csv", "html", "plain","xml","javascript"]
	audio=["mpeg"]
	vedio=["mp4"]
	image=["gif", "jpeg", "png"]
	if ext in application:
		content_typ="application" + "/" + ext
	elif ext in text:
		content_typ="text" + "/" + ext
	elif ext in audio:
		content_typ="audio" + "/" + ext
	elif ext in vedio:
		content_typ="vedio" + "/" + ext
	elif ext in image:
		content_typ="image" + "/" + ext
	else:
		content_typ="not recognized."
	return content_typ

#this will give status code if file present or not
def status_code(file_name):
	if os.path.isfile(file_name):
		status=200
	elif not os.path.exists(file_name):
		status=404
	else:
		status=400
	return status


def get_data(file_name):            #body
	f=open(file_name, 'rb')
	data=f.read()
	return data

#accesses log file
#adds any log to log file
def log_file(add, method, request_url, scode):

	#in log file there is ip address op requestes host, date&time and requested url
	log_date = current_datetime()
	address = str(add)
	ip_address = address.split("'")[1]
	method_type = method
	url = request_url
	s_code = scode
	f = open("/home/shreyas/Desktop/http/log_file.log", "a+")
	f.write(ip_address + "--[" + log_date + "] \"" +method_type + " " + url + "\" " + str(s_code) + "\n")
	f.close()

# adds error logs in log file
def error_log(add, method, request_url, scode):
	#accesses log file 
	#in log file there is ip address op requestes host, date&time and requested url
	log_date = current_datetime()
	address = str(add)
	ip_address = address.split("'")[1]
	method_type = method
	url = request_url
	s_code = scode
	f = open("/home/shreyas/Desktop/http/error_log.log", "a+")
	f.write("["+ log_date + "] [error] [client " + ip_address + "] " +str(status[s_code]) + " : " +method_type +" "+ url + "\n")
	f.close()

#sets new unique cookie for new client
def set_cookies(add):
	#if the new client is sending the request then assign the unique value to it
	#it can set one or multiple cookies to client
	ip_address = str(add).split("'")[1]
	
	#create the new cookie
	set_cookie = ""
	for i in range(10):
		set_cookie = set_cookie + random.choice(string.ascii_uppercase + string.digits)

	cf = open("/home/shreyas/Desktop/http/cookies.cookie", "a+")
	cf.write("Ip address = " + ip_address + " Cookie = " + set_cookie + "\n")
	cf.close
	
	return set_cookie


#end of the block is --boundary-- so before split the blocks remove the last part of boundary and store the blocks in list
def post_multipart(body, url, boundary):
	
	try:
		body_data = body
		end_ofblock = "\r\n"+boundary + "--\r\n"
		end_ofblock = end_ofblock.encode("utf-8")
		body_data.replace(end_ofblock, b'')
  
		#now split the data at boundary
		split_boundary = boundary + "\r\n"
		split_boundary = split_boundary.encode('utf-8')
		block = body_data.split(split_boundary)
  
		#save all the post data in a file
		#print(body_data)
		#print(split_boundry)
		#print(block)
		post_dictionary = {}
		for i in block:
			total_block = i.split(b'\r\n\r\n')
			block_header = total_block[0]
			if(len(total_block) <=1):
				continue
			block_data = total_block[1]
			block_header = block_header.decode('utf-8')
			block_headers = block_header.split('; ')
			headers_dict = {}
   
			#get the name and file name from the header and write the data according to given filename
			for fields in block_headers:
				if '=' in fields:
					key = fields.split('=')[0]
					value = fields.split('=')[1]
					headers_dict[key] = value
					#print(value)
			
			#if the content is file then store it in the given filename 
			#otherwise just add value of the field name in json file 
			if("filename" in headers_dict.keys()):
				fieldvalue = "\\home\\shreyas\\Desktop\\http\\" + headers_dict["filename"]
				file = open(filename, "wb")
				file.write(block_data)
				file.close()
			else:
				fieldvalue = block_data.decode('utf-8')
				fieldvalue = fieldvalue.split('\r\n')[0]

			fieldname = headers_dict["name"]
			post_dictionary["name"] = fieldname
			post_dictionary["value"] = fieldvalue
			f = open("mul_post.json","a+")
			f.write(json.dumps(post_dictionary))
			f.close()
		
		s_code = 200
		head = "HTTP/1.1" + " 200 " + str(status[200]) + CRLF
  
	except:
		s_code = 400
		head = "HTTP/1.1" + " 400 " + str(status[400]) + CRLF

	return s_code, head

#will create responce for http requests
def CreateResponse(clientconnection,add):
	"""if recieving data is greater than 1024 bytes then other data will be lost so 
	receving data in parts and join the whole data so that no data is lost"""
	parts = []
	CRLF = "\r\n"
	while True:
		rec_request=clientconnection.recv(limit)
		parts.append(rec_request)
		if(len(rec_request) < limit):
			break

	rec_request = b''.join(parts)

	#split thte headers and data
	global conn_available
	r = rec_request.split(b'\r\n\r\n', 1)
	rh = r[0].decode("utf-8")
	#now split each header and store it in a dictionary
	headers = rh.split('\r\n')
	request=headers[0].split(' ')
	request_headers= {}
	for lines in headers[1:]:
		m = lines.split(': ')
		request_headers[m[0]] = m[1]
	#print(request_headers)
	#print(type(r[1]))
	#print(re)
	response_headers = {}
	if("Cookie" not in request_headers.keys()):
		#set the cookie value
		response_headers["Set-Cookie"] = set_cookies(add) 

	#Get request
	if(request[0]=="GET"):                                        #GET
		#print(request_headers)
		header = {}
		CRLF = "\r\n"
		url=request[1]
		url= url.split('?')[0]
		response_headers["Date"] = current_datetime()
		response_headers["Server"] = "Localhost"
		response_headers["connection"] = "close"
		print("GET request->\n")
		#if request url is in invalid format it return bad request
		if (len(request) < 3 or request[2][:4] != "HTTP" or (url[:6] != "http//" and url[0] != "/")):
			s_code = 400
			head = "HTTP/1.1 400 " + str(status[s_code]) +CRLF
		else:
			file_name=url[url.rfind("/")+1:]
			#print(file_name)
			s_code = status_code(file_name)
			head= "HTTP/1.1 " + str(status_code(file_name)) +" " + status[status_code(file_name)] + CRLF
			if os.path.exists(os.path.dirname(url)):
				if(status_code(file_name) == 200):
					last_modified_time = os.path.getmtime(file_name)
					last_modified_time = datetime.datetime.fromtimestamp(last_modified_time, datetime.timezone.utc).replace(microsecond=0 ,tzinfo=None)
					#print(last_modified_time)
					#conditional GET 
					"""if modified since = if the header is present in request then check the last modified date of the 
					file request and time in header .if the time in header is greter  than last modified time is
					then send respond as 304 not modified else continue"""
					MODIFIED = 0
					if ("If-Modified-Since" in request_headers.keys()):
						if_modified_since_time = request_headers["If-Modified-Since"]
						if_modified_since_time = datetime.datetime.strptime(if_modified_since_time, "%a, %d %b %Y %X %Z")
						if (last_modified_time <= if_modified_since_time):
							MODIFIED = 1
							s_code=304
							head = request[2] + " 304 " + str(status[304]) + CRLF
					#if request header contain if-unmodified since header then compare the time with last modified
					#if last modifed is greter than time in header send response code 412
					elif("If-Unmodified-Since" in request_headers.keys()):
						if_unmodified_since_time = request_headers["If-Unmodified-Since"]
						if_unmodified_since_time = datetime.datetime.strptime(if_unmodified_since_time, "%a, %d %b %Y %X %Z")
						if (last_modified_time > if_unmodified_since_time):
							MODIFIED = 1
							s_code = 412
							head = request[2] + " 412 " + str(status[412]) + CRLF

					if(MODIFIED == 0):
						s_code = 200
						header["Date"] = current_datetime()
						header["content-type"]=content_type(url.split('.', 1)[-1])
						header["Server"] = "Localhost"
						header["Last-modified"] = modified_datetime(file_name)
						data= get_data(file_name)
						header["content-length"] = len(data)
						header["connection"] = "close"
						for key in header:
							head = head + "{} : {}{}".format(key,str(header[key]),CRLF)

						head = head.encode("utf-8") + "\r\n".encode("utf-8") + data
			else:
				s_code = 400
				head = request[2] + " 400 " + str(status[400]) + CRLF
				print("path is incorrect")
		log_file(add, request[0], request[1],s_code)
  
		if (s_code == 200):
			clientconnection.send(head)
		else:
			for key in response_headers:
				head = head + "{} : {}{}".format(key,str(response_headers[key]),CRLF)

			head = head + CRLF
			clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1

	#HEAD
	elif(request[0]=="HEAD"):                                              
		header = {}
		CRLF = "\r\n"
		url=request[1]
		url = url.split('?')[0]
		response_headers["Date"] = current_datetime()
		response_headers["Server"] = "Localhost"
		response_headers["connection"] = "close"
		print("HEAD request->\n")
		if (len(request) < 3 or request[2][:4] != "HTTP" or (url[:6] != "http//" and url[0] != "/")):
			s_code = 400
			head = "HTTP/1.1 400 " + str(status[s_code]) + CRLF
		else:
			file_name=url[url.rfind("/")+1:]
			s_code = status_code(url)
			head= "HTTP/1.1 " + str(status_code(file_name)) +" " + status[status_code(file_name)] + CRLF
			if(status_code(file_name) == 200):
				s_code = 200
				header["Date"] = current_datetime()
				header["content-type"]=content_type(url.split('.', 1)[-1])
				header["Server"] = "Localhost"
				header["Last-modified"] = modified_datetime(file_name)
				data= get_data(file_name)
				header["content-length"] = len(data)
				header["connection"] = "close"
				for key in header:
					head = head + "{} : {}{}".format(key,str(header[key]),CRLF)

		log_file(add, request[0], request[1],s_code)
		if (s_code == 200):
			head = head + CRLF
			clientconnection.send(head.encode("utf-8"))
		else:
			for key in response_headers:
				head = head + "{} : {}{}".format(key,str(response_headers[key]),CRLF)

			head = head + CRLF
			clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1

	#POST method
	elif(request[0]=="POST"):
		url = request[1]
		url = url.split('?')[0]
		file_name=url[url.rfind("/")+1:]
		temp_dict = {}
		response_headers["Date"] = current_datetime()
		response_headers["Server"] = "Localhost"
		response_headers["connection"] = "close"
		#print(request_headers)
		print("POST request->\n")
		if(request_headers['Content-Type'] == "application/x-www-form-urlencoded"):
			body = r[1].decode('utf-8')
			lines = body.split('&')
			for i in lines:
				d = i.split('=')
				temp_dict[d[0]] = d[1]
			#print(temp_dict)
		elif ("multipart/form-data" in request_headers['Content-Type']):
			contenttype = request_headers['Content-Type'].split('; boundary=')
			#print(request_headers['Content-Type'])
			#print(contenttype)
			boundary = contenttype[1]
			boundary = "--" + boundary
			if os.path.exists(os.path.dirname(url)):
				if("Content-Length" not in request_headers.keys()):
					s_code = 411
					head = request[2] + " 411 " + str(status[411]) + CRLF
				elif(len(r[1]) != int(request_headers["Content-Length"])):
					s_code = 400
					head = request[2] + " 400 " + str(status[400]) + CRLF
					print("content lenght not matched")
				else:
					body_to_mul = r[1]
					s_code , head = post_multipart(body_to_mul, url, boundary)
				
			else:
				s_code = 404
				head =request[2] + " 404 " + str(status[404]) + CRLF
				print("path not exist")

		else:
			s_code = 415
			head = request[2] + " 415 " + str(status[415]) + CRLF
			print("Unsupported Media Type")

		if (len(request) < 3 or request[2][:4] != "HTTP" or (url[:6] != "http//" and url[0] != "/")):
			s_code = 400
			head = "HTTP/1.1 400 " + str(status[s_code]) + CRLF
		elif ("Content-Length" not in request_headers.keys()):
				s_code = 411
				head = request[2] + " 411 " + str(status[411]) + CRLF
		else:
			if(request_headers['Content-Type'] == "application/x-www-form-urlencoded"):
				if (os.path.exists(url)):
					print("file exists")
					#file is exist appending the data in file
					extention = file_name.split('.')[1]

					if (extention == "json"):
						json_file = open(url,"a+")
						json_file.write(json.dumps(temp_dict))
						json_file.close()
						s_code = 200
						head = request[2] + " 200 " + str(status[200]) + CRLF
					else:
						s_code = 400
						head = request[2] + " 400 " + str(status[400]) + CRLF
						print("wrong file type")
				else:
					#file is not exist
					#check the path before file is present 
					if os.path.exists(os.path.dirname(url)):
						extention = file_name.split('.')[1]
						if( extention == "json"):
							print("file not exist. creating new file...")
							json_file = open(url,"a+")
							json_file.write(json.dumps(temp_dict))
							json_file.close()
							s_code = 201
							head = request[2] + " 201 " + str(status[201]) + CRLF
						else:
							s_code = 400
							head = request[2] + " 400 " + str(status[400]) + CRLF
							print("wrong file type")

					else:
						s_code = 404
						head =request[2] + " 404 " + str(status[404]) + CRLF
						print("path not exist")
		for key in response_headers:
			head = head + "{} : {}{}".format(key,str(response_headers[key]),CRLF)

		head = head + CRLF
		log_file(add, request[0], request[1],s_code)	
		clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1
		
	#PUT method
	elif(request[0] == "PUT"):
		url_main = request[1]
		url = url_main.split('?')[0]
		file_name=url[url.rfind("/")+1:]
		body = r[1]
		CRLF = "\r\n"
		response_headers["Date"] = current_datetime()
		response_headers["Server"] = "Localhost"
		response_headers["connection"] = "close"
		#check the the file path is exist or not
		print("PUT request ->\n")
		if (len(request) < 3 or request[2][:4] != "HTTP" or (url[:6] != "http//" and url[0] != "/")):
			s_code = 400
			head = "HTTP/1.1 400 " + str(status[s_code]) + CRLF
		else:
			if ("Content-Length" not in request_headers.keys()):
				s_code = 411
				head = request[2] + " 411 " + str(status[411]) + CRLF
				#print("content lenght is required.")
			else:
				len_body = len(body)
				if(int(request_headers["Content-Length"]) != len_body):
					#if the content lenght from request header is not equal to body length is  bad request
					s_code = 400
					head = request[2] +" 400 " + str(status[s_code]) + CRLF
					print("given content-length header value and body length not matched")
				elif os.path.exists(os.path.dirname(url)):
					#check the given path is file not only directory
					if not os.path.isdir(url):
						#now check the file is exist or not
						if os.path.exists(file_name):
							#check the permission 
							
							s_code = 200
							#file exist overwrite the content of body
							f = open(url, "wb")
							f.write(body)
							f.close()
							head = request[2] + " 200 " + str(status[200]) +CRLF
						
						else:
							
							s_code = 201
							#file created and added content
							f = open(url, "wb")
							f.write(body)
							f.close()
							head = request[2] + " 201 " + str(status[201]) + CRLF
							

					else:
						s_code = 403
						head = request[2] + " 403 " + str(status[403]) + CRLF
						print("path is directory not file")
						#file is not exist create new file and write the content 

				else:
					s_code = 404
					head = request[2] + " 404 " + str(status[404]) + CRLF
					print("path not exist")

		log_file(add, request[0], request[1],s_code)
		for key in response_headers:
			head = head + "{} : {}{}".format(key,str(response_headers[key]),CRLF)

		head = head + CRLF
		
		clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1
	#DELETE method
	elif(request[0] == "DELETE"):
		url = request[1]
		url = url.split('?')[0]
		file_name=url[url.rfind("/")+1:]
		body = r[1]
		CRLF = "\r\n"
		response_headers["Date"] = current_datetime()
		response_headers["Server"] = "Localhost"
		response_headers["connection"] = "close"
		print("DELETE request ->\n")
		if (len(request) < 3 or request[2][:4] != "HTTP" or (url[:6] != "http//" and url[0] != "/")):
			s_code = 400
			head = "HTTP/1.1 400 " + str(status[s_code]) + CRLF
		else:
			if os.path.exists(os.path.dirname(url)):
				#check the given path is file not only directory
				if not os.path.isdir(url):
					#now check the file is exist or not
					if os.path.exists(url):
						if os.access(url, os.W_OK):
							s_code = 200
							os.remove(url)#delete the file 
							head = request[2] + " 200 " + str(status[200]) +CRLF
							print("file deleted.")
						else:
							s_code = 403
							head = request[2] + " 403 " + str(status[403]) +CRLF
							print("permission denied")
					else:
						#file didn't exist
						s_code = 404
						head = request[2] + " 404 " + str(status[404]) + CRLF
						print("file not found")


				else:
					s_code = 403
					head = request[2] + " 403 " + str(status[403]) + CRLF
					print("directory can't be deleted")

			else:
				s_code = 404
				head = request[2] + " 404 " + str(status[404]) + CRLF
				print("path didn't exists")

		log_file(add, request[0], request[1],s_code)
		for key in response_headers:
			head = head + "{} : {}{}".format(key,str(response_headers[key]),CRLF)

		head = head + CRLF
		
		clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1

	else:
		s_code = 501              #Method Not Implemented
		head = "HTTP/1.1 501 " + str(status[s_code])
		clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
		conn_available -= 1

	print("response ->" + str(s_code))
	error_s_code = [400,403,404,411,412]
	if s_code in error_s_code:
		error_log(add,request[0], request[1],s_code)

while True:
	clientconnection, add = serversocket.accept()
	conn_available += 1
	print('New request accepted from')
	print(add)
	print(clientconnection)
	print()
	print(conn_available)
	#maximum simultaneous connection available at a time is 10
	#if more the 10 connection are trying to connect then server give response it is busy 
	CRLF ="\r\n"
	if(conn_available < int(config["MaxRequests"])):
		th = threading.Thread(target = CreateResponse, args =(clientconnection,add,))
		th.start()
	else:
		print("server is busy")
		#status code 503 is responded when server is overloaded or under maintainance 
		head = "HTTP/1.1 503 Server Unavailable" + CRLF + CRLF
		clientconnection.send(head.encode("utf-8"))
		clientconnection.close()
